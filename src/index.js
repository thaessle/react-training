import React from 'react';
import ReactDOM from 'react-dom';
import MissionsPage from './components/missions-page';

ReactDOM.render(
  <MissionsPage/>,
  document.getElementById('main')
);
