# React Training Boilerplate

Training project to build API backend services

## Informations

### What you need to start :
* [nodeJS v6.9.x & NPM](https://nodejs.org/en/download/) :
* [Git](https://git-scm.com/)
* [Gitlab account](https://gitlab.com/)

Clone the project : `git clone git@gitlab.com:thaessle/express-boilerplate.git`

Install the dependencies : `npm i`

Let's hacking : `npm run dev`

### Libraries to use in this project :
* [Babel](https://babeljs.io/) : JavaScript Polyfill for ES2015, ES2016, ES2017
* [React](https://facebook.github.io/react/) : Fast, unopinionated, minimalist web framework for node.js
* [react-router](https://facebook.github.io/react/) : routing library for react
* [styled-component](styled-components) : visual primitives for the components
* [webpack](https://webpack.github.io/docs/) : module builder
* Unit test kit :
  * [mocha](https://mochajs.org/) :  test framework running on node.js and in the browser
  * [chai](http://chaijs.com/) : assertion library
  * [enzyme](http://airbnb.io/enzyme/) : testing utility for React
  * [nyc](https://github.com/istanbuljs/nyc) : Istanbul command line interface

### NPM Scripts :
* Run unit tests : `npm run test`
* Run linters : `npm run lint`
* Run platform for dev with hot reload : `npm run dev`
* Build platform for production : `npm run build`
* Run platform for production : `npm start`


## Exercice
1. Create a < Mission > component with tests
  * Should contain a picture, a title and a short description
2. Adapt < MissionsPage > to show a list of < Mission >
  * Number of Mission must depend on a MissionPage property
3. Create a < MissionDetails > component with tests
4. Use react-router to navigate to < MissionDetails > when clicking on a < Mission > in < MissionPage >
5. Use Styled Components to style your components

## Developer materials

Some stuff to be a better hacker

![80's hacker](http://i.giphy.com/26vUBzVWEBkk3KD6M.gif)

### Usefull tools :
* [Chrome DevTools](https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi) : React debugging tool in Chrome
* [Gitmoji](https://gitmoji.carloscuesta.me/) : emojis for your commit messages
